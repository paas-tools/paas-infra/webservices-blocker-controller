# Web Services blocker controller

This go controller takes care of `blocking` and `unblocking` OpenShift projects as instructed by [CERN WebServices](cern.ch/web). The controller controls namespaces that need to be blocked or unblocked through annotations. These annotations are set by [OpenShift WebServices API](https://gitlab.cern.ch/paas-tools/openshift-app-manager).

Technically, blocking a site consists of:
1. Excluding routes from routers, making impossible to reach the site from outside OpenShift (via `router.cern.ch/exclude` label on Routes)
2. [Idling](https://docs.openshift.org/latest/admin_guide/idling_applications.html) all services in the project.
3. Changing the maximum number of pods in the quota to `0`
4. Removing all remaining pods. This will take care of pods which are not behind a Service.

On the other hand, unblocking consists of:

1. Add previously-excludes routes back to the routers
2. Reverting back the pod quota to the previous value

### Deployment
The controller is deployed in both `openshift.cern.ch` and `openshift-dev.cern.ch` in the `paas-infra` namespace.
For instructions on how to deploy the namespace, check the [OneNote docs](https://espace.cern.ch/openshift-internal/_layouts/15/WopiFrame.aspx?sourcedoc=%2Fopenshift%2Dinternal%2FShared%20Documents%2FOpenshift&action=edit&wd=target%28%2F%2FDeployment.one%7Cdacb26c0-65a0-4eee-8b63-99cfeaef4a66%2F%27paas-infra%27%20namespace%7C9a78943f-4902-48c6-a972-2a5b18e8e901%2F%29)

The account running the webservices-blocker-controller needs permissions to edit all namespaces,
so we are going to assign it the `cluster-admin` role.
```
oc create serviceaccount webservices-blocker-controller -n paas-infra
oc adm policy add-cluster-role-to-user cluster-admin -z webservices-blocker-controller -n paas-infra
```

Once this is done, just create the application by running:
```
oc create -f deploy/ -n paas-infra
```

### Continuous Integration

The deployment of the application is managed by GitLab-CI. Any changes to the definition and config
files will be automatically redeployed into dev and production when merged to `master`.

For the development purposes, two manual triggers are included in the CI definition to allow easy deployment of
custom branches into the application running in the dev [environment](https://openshift-dev.cern.ch).

Check the [OneNote docs](https://espace.cern.ch/openshift-internal/_layouts/15/WopiFrame.aspx?sourcedoc=%2Fopenshift%2Dinternal%2FShared%20Documents%2FOpenshift&action=edit&wd=target%28%2F%2FDeployment.one%7Cdacb26c0-65a0-4eee-8b63-99cfeaef4a66%2F%27paas-infra%27%20namespace%7C9a78943f-4902-48c6-a972-2a5b18e8e901%2F%29) for instructions on how to set up the Continuous Integration and Continuous Deployment for this project.
