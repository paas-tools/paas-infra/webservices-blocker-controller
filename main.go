/*
Copyright 2017 The Kubernetes Authors.

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/

package main

import (
	"fmt"
	"strconv"
	"time"

	"k8s.io/apimachinery/pkg/api/resource"

	goflag "flag"

	appsv1 "github.com/openshift/client-go/apps/clientset/versioned/typed/apps/v1"
	routev1 "github.com/openshift/client-go/route/clientset/versioned/typed/route/v1"

	"github.com/golang/glog"
	flag "github.com/spf13/pflag"
	"k8s.io/api/core/v1"
	metav1 "k8s.io/apimachinery/pkg/apis/meta/v1"
	"k8s.io/apimachinery/pkg/fields"
	"k8s.io/apimachinery/pkg/util/runtime"
	"k8s.io/apimachinery/pkg/util/wait"
	"k8s.io/client-go/kubernetes"
	"k8s.io/client-go/rest"
	"k8s.io/client-go/tools/cache"
	"k8s.io/client-go/util/workqueue"
)

// Constants for this controller
const (
	webservicesBlockedAnnotation                             = "cern.ch/webservices-blocked"
	webservicesBlockedStatusAnnotation                       = "cern.ch/webservices-blocked-status"
	webservicesBlockedStatusAnnotationPending                = "Pending"
	webservicesBlockedStatusAnnotationCompleted              = "Completed"
	webservicesBlockedStatusAnnotationToUnblock              = "ToUnblock"
	webservicesBlockedRoutebyWebServicesControllerAnnotation = "webservices-blocker.cern.ch/blocked-route"
	routeExcludedAnnotation                                  = "router.cern.ch/exclude"
	webservicesBlockedScaledDownAnnotation                   = "webservices-blocker.cern.ch/old-pod-replica-number"
	webservicesBlockedPodQuotaAnnotation                     = "webservices-blocker.cern.ch/old-pod-quota"
	podTerminationGracePeriod                                = 30 * time.Second
)

// Dry run mode
var dryRun bool

type Controller struct {
	indexer  cache.Indexer
	queue    workqueue.RateLimitingInterface
	informer cache.Controller
	client   kubernetes.Interface
}

func NewController(queue workqueue.RateLimitingInterface, indexer cache.Indexer, informer cache.Controller, client kubernetes.Interface) *Controller {
	return &Controller{
		informer: informer,
		indexer:  indexer,
		queue:    queue,
		client:   client,
	}
}

func (c *Controller) processNextItem() bool {
	// Wait until there is a new item in the working queue
	key, quit := c.queue.Get()
	if quit {
		return false
	}
	// Tell the queue that we are done with processing this key. This unblocks the key for other workers
	// This allows safe parallel processing because two objects with the same key are never processed in
	// parallel.
	defer c.queue.Done(key)

	// Invoke the method containing the business logic
	err := c.doWork(key.(string))
	// Handle the error if something went wrong during the execution of the business logic
	c.handleErr(err, key)
	return true
}

// doWork is the business logic of the controller. In case an error happened, it has to simply return the error.
// The retry logic should not be part of the business logic.
func (c *Controller) doWork(key string) error {
	obj, exists, err := c.indexer.GetByKey(key)
	if err != nil {
		glog.Errorf("Fetching object with key %s from store failed with %v", key, err)
		return err
	}

	if !exists {
		glog.Infof("Namespace %s does not exist anymore\n", key)
		return nil
	}

	namespace := obj.(*v1.Namespace)
	needToUpdate := false

	// Check if the namespace has been blocked, ignore otherwise
	if _, ok := namespace.Annotations[webservicesBlockedAnnotation]; ok {

		blockedStatus, ok := namespace.Annotations[webservicesBlockedStatusAnnotation]
		if !ok {
			// If there is 'webservicesBlockedAnnotation' but no 'webservicesBlockedStatusAnnotation',
			// assume it is Pending. This is for compatibility with the old blocking method
			// written in Python
			blockedStatus = webservicesBlockedStatusAnnotationPending
		}

		// Depending on status, block, unblock or do nothing.
		switch status := blockedStatus; status {
		case webservicesBlockedStatusAnnotationPending:
			// If the webservicesBlockedStatusAnnotation annotation has no value or it is
			// 'Pending', proceed to block the site

			glog.Infof("Blocking namespace '%s'!\n", namespace.GetName())
			if dryRun {
				glog.Infof("'DRY-RUN: Namespace: '%s' NOT blocked", namespace.GetName())
				return nil
			}
			if err := c.block(namespace); err != nil {
				return err
			}
			// Set completed annotation
			namespace.Annotations[webservicesBlockedStatusAnnotation] = webservicesBlockedStatusAnnotationCompleted
			needToUpdate = true
			glog.Infof("Namespace '%s' successfully blocked!\n", namespace.GetName())
		case webservicesBlockedStatusAnnotationToUnblock:
			// If the annotation is set to the value of 'unblock', proceed to unblock the site
			glog.Infof("Unblocking namespace '%s'!\n", namespace.GetName())
			if dryRun {
				glog.Infof("'DRY-RUN: Namespace: '%s' NOT unblocked", namespace.GetName())
				return nil
			}
			if err := c.unblock(namespace); err != nil {
				return err
			}
			delete(namespace.Annotations, webservicesBlockedStatusAnnotation)
			delete(namespace.Annotations, webservicesBlockedAnnotation)
			needToUpdate = true
			glog.Infof("Namespace '%s' successfully unblocked!\n", namespace.GetName())
		case webservicesBlockedStatusAnnotationCompleted:
			// Blocking completed, nothing to do
		default:
			return fmt.Errorf("unknown blocking status for namespace '%s': '%s', aborting... ",
				namespace.GetName(), status)
		}
	}

	if needToUpdate {
		// If annotations were changed, update namespace
		namespaceClient := c.client.CoreV1().Namespaces()
		if n, err := namespaceClient.Update(namespace); err != nil {
			return err
		} else {
			glog.Infof("Namespace '%s'updated!\n", n.GetName())
		}
	}
	return nil
}

// Blocking consists stopping all pods and not allowing the user to create them back. To do so:
// 1. All routes in the project are excluded from the routers
// 2. All the scalable objects are scaled to 0. This will stop all permantent running pods.
// 3. Set the project quota max number of pods to 0. This will prevent pods not linked to a service
//    (e.g. Cronjobs) to start
// 4. Wait 5 seconds and delete all pods (if there are any left). This will ensure all pods are gone
func (c *Controller) block(namespace *v1.Namespace) error {

	// Initialize route and quota clients for blocking
	config, err := rest.InClusterConfig()
	if err != nil {
		return err
	}
	routeV1Client, err := routev1.NewForConfig(config)
	if err != nil {
		return err
	}

	// Exclude all routes from routers
	// oc label routes --all router.cern.ch/exclude=true --overwrite --namespace …
	glog.Infof("Excluding routes from routers in namespace '%s'\n", namespace.GetName())
	routes, err := routeV1Client.Routes(namespace.GetName()).List(metav1.ListOptions{})
	if err != nil {
		return err
	}
	for _, route := range routes.Items {
		// Do not re-exclude routes already excluded. When/If the project is unblocked,
		// we want to only unexclude routes blocked by us. If there are other routes blocked by other reasons,
		// we ignore them (we use annotation )
		if route.Labels == nil || route.Labels[routeExcludedAnnotation] != "true" {
			glog.Infof("Proceding to exclude route %s/%s ...\n", route.GetNamespace(), route.GetName())
			if route.Labels == nil {
				route.Labels = make(map[string]string)
			}
			route.Labels[routeExcludedAnnotation] = "true"
			if route.Annotations == nil {
				route.Annotations = make(map[string]string)
			}
			route.Annotations[webservicesBlockedRoutebyWebServicesControllerAnnotation] = "true"
			// Save the route after being excluded from routers
			_, err := routeV1Client.Routes(namespace.GetName()).Update(&route)
			if err != nil {
				return err
			}
			glog.Infof("Route %s/%s successfully excluded\n", route.GetNamespace(), route.GetName())
		}
	}
	glog.Infof("Routes successfully excluded from routers in namespace '%s'\n", namespace.GetName())

	// Scale down all scalable resources
	// At the moment, only DeploymentConfigs and Deployments are considered scalable objects
	glog.Infof("Scale down all scalable resources in '%s'\n", namespace.GetName())
	// First do DeploymentConfigs
	appsv1, err := appsv1.NewForConfig(config)
	if err != nil {
		return err
	}
	deploymentConfigs, err := appsv1.DeploymentConfigs(namespace.GetName()).List(metav1.ListOptions{})
	if err != nil {
		return err
	}
	for _, dc := range deploymentConfigs.Items {
		if dc.Spec.Replicas > 0 {
			glog.Infof("Proceding to scale down deployment config %s/%s ...\n", dc.GetNamespace(), dc.GetName())
			if dc.Annotations == nil {
				dc.Annotations = make(map[string]string)
			}
			dc.Annotations[webservicesBlockedScaledDownAnnotation] = fmt.Sprint(dc.Spec.Replicas)
			dc.Spec.Replicas = 0
			_, err := appsv1.DeploymentConfigs(namespace.GetName()).Update(&dc)
			if err != nil {
				return err
			}
			glog.Infof("Deployment config %s/%s successfully scaled down\n", dc.GetNamespace(), dc.GetName())
		}
	}

	// Do the same for Kubernetes' deployment objects
	deploymentClient := c.client.ExtensionsV1beta1().Deployments(namespace.GetName())
	deployments, err := deploymentClient.List(metav1.ListOptions{})
	if err != nil {
		return err
	}
	for _, deployment := range deployments.Items {
		if *deployment.Spec.Replicas > 0 {
			glog.Infof("Proceding to scale down deployment %s/%s ...\n", deployment.GetNamespace(), deployment.GetName())
			if deployment.Annotations == nil {
				deployment.Annotations = make(map[string]string)
			}
			deployment.Annotations[webservicesBlockedScaledDownAnnotation] = fmt.Sprint(deployment.Spec.Replicas)
			newReplicaCount := int32(0)
			deployment.Spec.Replicas = &newReplicaCount
			_, err := deploymentClient.Update(&deployment)
			if err != nil {
				return err
			}
			glog.Infof("Deployment %s/%s successfully scaled down\n", deployment.GetNamespace(), deployment.GetName())
		}
	}

	resourceQuotaClient := c.client.CoreV1().ResourceQuotas(namespace.GetName())
	quota, err := resourceQuotaClient.Get(fmt.Sprintf("%s-quota", namespace.GetName()), metav1.GetOptions{})
	if err != nil {
		return err
	}

	// Ensure it has a valid annotations map
	if quota.Annotations == nil {
		quota.Annotations = make(map[string]string)
	}
	// Only proceed if the quota has not already been blocked
	if _, exists := quota.Annotations[webservicesBlockedPodQuotaAnnotation]; !exists {
		quota.Annotations[webservicesBlockedPodQuotaAnnotation] = quota.Spec.Hard.Pods().String()
		// Change pod quota to 0, so no new pods can be created anymore
		quota.Spec.Hard[v1.ResourcePods] = resource.MustParse("0")

		// Save changes to Resource quota
		_, err = resourceQuotaClient.Update(quota)
		if err != nil {
			return err
		}
		glog.Infof("Pod quota in namespace '%s' successfully set to 0\n", namespace.GetName())
	} else {
		glog.Infof("Pod quota is already blocked, proceeding...")
	}

	// Wait a bit (for idle to take efect and remove old pods)
	podClient := c.client.CoreV1().Pods(namespace.GetName())
	pods, err := podClient.List(metav1.ListOptions{})
	if err != nil {
		return err
	}
	if len(pods.Items) > 0 {
		// There are pods to be deleted. Wait a bit before proceeding, to give time to idling to work.
		glog.Infof("There are still pods running in namespace %s, proceeding to delete them...", namespace.GetName())
		time.Sleep(5 * time.Second)
		for _, pod := range pods.Items {
			if err := podClient.Delete(pod.GetName(), &metav1.DeleteOptions{}); err != nil {
				// Do not abort if pod fails to be deleted
				glog.Errorln(err)
			}
			glog.Infof("Pod: %s successfully deleted", pod.GetName())
		}
		// Check again if all pods are gone. If so, gracefully terminate,
		// if not, do it again in the next iteration
		time.Sleep(podTerminationGracePeriod)
		pods, err := podClient.List(metav1.ListOptions{})
		if err != nil {
			return err
		}
		if len(pods.Items) > 0 {
			return fmt.Errorf("There are still pods after the grace period, aborting. Namespace '%s' will be retried",
				namespace.GetName())
		}
	}
	return nil
}

// Unblocking consists on re-scaling the replication controllers back to their previous value.
func (c *Controller) unblock(namespace *v1.Namespace) error {

	// Initialize route and quota clients for unblocking
	config, err := rest.InClusterConfig()
	if err != nil {
		return err
	}
	routeV1Client, err := routev1.NewForConfig(config)
	if err != nil {
		return err
	}

	routes, err := routeV1Client.Routes(namespace.GetName()).List(metav1.ListOptions{})
	if err != nil {
		return err
	}

	glog.Infof("Include routes into routers in namespace '%s'\n", namespace.GetName())
	for _, route := range routes.Items {
		// Only reenable routes that were excluded by us
		if route.Labels != nil && route.Labels[webservicesBlockedRoutebyWebServicesControllerAnnotation] != "true" {
			delete(route.Labels, routeExcludedAnnotation)
			delete(route.Annotations, webservicesBlockedRoutebyWebServicesControllerAnnotation)
			// Save the route after removing the label and annotation
			_, err := routeV1Client.Routes(namespace.GetName()).Update(&route)
			if err != nil {
				return err
			}
		}
	}
	glog.Infof("Routes successfully included to routers in namespace '%s'\n", namespace.GetName())

	// Restore quota
	// old_pods=`oc get resourcequota/test-rodrigal-playground-quota -o jsonpath="{.metadata.annotations.old-pod-quota}"`
	// oc annotate resourcequota --all old-pod-quota-
	// oc patch resourcequota --all -p "{\"spec\":{\"hard\":{\"pods\":\"$old_pods\"}}}"
	glog.Infof("Reverting pod quota back to previous value for namespace '%s'\n", namespace.GetName())

	resourceQuotaClient := c.client.CoreV1().ResourceQuotas(namespace.GetName())
	quota, err := resourceQuotaClient.Get(fmt.Sprintf("%s-quota", namespace.GetName()), metav1.GetOptions{})
	if err != nil {
		return err
	}

	// Recover oldQuotaCount from annotation
	oldPodCount, exists := quota.Annotations[webservicesBlockedPodQuotaAnnotation]
	if exists {
		parsedQuota, err := resource.ParseQuantity(oldPodCount)
		if err != nil {
			glog.Errorf("Malformed annotation '%s' in quota object. Pod quota in namespace '%s' cannot be restored. Continuing to unblock project...",
				webservicesBlockedPodQuotaAnnotation, quota.GetNamespace())
		} else {
			// Change pod quota back to old value
			quota.Spec.Hard[v1.ResourcePods] = parsedQuota
			// Delete oldPodCount annotation
			delete(quota.Annotations, webservicesBlockedPodQuotaAnnotation)

			// Save changes to Resource quota
			_, err = resourceQuotaClient.Update(quota)
			if err != nil {
				return err
			}
			glog.Infof("Pod quota in namespace '%s' successfully reverted back to %s\n", namespace.GetName(), oldPodCount)
		}
	} else {
		glog.Errorf("Annotation '%s' in quota of namespace '%s' does not exist. Will continue to unblock without changing the quota",
			webservicesBlockedPodQuotaAnnotation, namespace.GetName())
	}

	// Recover value from scalable resource, if any
	// First do DeploymentConfigs
	appsv1, err := appsv1.NewForConfig(config)
	if err != nil {
		return err
	}
	deploymentConfigs, err := appsv1.DeploymentConfigs(namespace.GetName()).List(metav1.ListOptions{})
	if err != nil {
		return err
	}
	for _, dc := range deploymentConfigs.Items {
		if dc.Annotations == nil {
			continue // If no annotations, ignore this particula dc
		}
		if oldReplicaCount, exists := dc.Annotations[webservicesBlockedScaledDownAnnotation]; exists {
			glog.Infof("Proceding to scale up deployment config %s/%s ...\n", dc.GetNamespace(), dc.GetName())
			value, err := strconv.ParseInt(oldReplicaCount, 10, 32)
			if err != nil {
				glog.Errorf("The old replica count stored in annotations '%s' is malformed. As we cannot scale up again, ignoring the error...",
					webservicesBlockedScaledDownAnnotation)
				return nil
			}
			dc.Spec.Replicas = int32(value)
			delete(dc.Annotations, webservicesBlockedScaledDownAnnotation)
			_, err = appsv1.DeploymentConfigs(namespace.GetName()).Update(&dc)
			if err != nil {
				return err
			}
			glog.Infof("Deployment config %s/%s successfully scaled up\n", dc.GetNamespace(), dc.GetName())
		}
	}

	// Do the same for Kubernetes' deployment objects
	deploymentClient := c.client.ExtensionsV1beta1().Deployments(namespace.GetName())
	deployments, err := deploymentClient.List(metav1.ListOptions{})
	if err != nil {
		return err
	}
	for _, deployment := range deployments.Items {
		if deployment.Annotations == nil {
			continue // If no annotations, ignore this particula dc
		}
		if oldReplicaCount, exists := deployment.Annotations[webservicesBlockedScaledDownAnnotation]; exists {
			glog.Infof("Proceding to scale up deployment %s/%s ...\n", deployment.GetNamespace(), deployment.GetName())
			value, err := strconv.ParseInt(oldReplicaCount, 10, 32)
			if err != nil {
				glog.Errorf("The old replica count stored in annotations '%s' is malformed. As we cannot scale up again, ignoring the error...",
					webservicesBlockedScaledDownAnnotation)
				return nil
			}
			i32value := int32(value)
			deployment.Spec.Replicas = &i32value
			delete(deployment.Annotations, webservicesBlockedScaledDownAnnotation)
			_, err = deploymentClient.Update(&deployment)
			if err != nil {
				return err
			}
			glog.Infof("Deployment %s/%s successfully scaled up\n", deployment.GetNamespace(), deployment.GetName())
		}
	}

	return nil
}

// handleErr checks if an error happened and makes sure we will retry later.
func (c *Controller) handleErr(err error, key interface{}) {
	if err == nil {
		// Forget about the #AddRateLimited history of the key on every successful synchronization.
		// This ensures that future processing of updates for this key is not delayed because of
		// an outdated error history.
		c.queue.Forget(key)
		return
	}

	// This controller retries 5 times if something goes wrong. After that, it stops trying.
	if c.queue.NumRequeues(key) < 5 {
		glog.Infof("Error syncing object %v: %v", key, err)

		// Re-enqueue the key rate limited. Based on the rate limiter on the
		// queue and the re-enqueue history, the key will be processed later again.
		c.queue.AddRateLimited(key)
		return
	}

	c.queue.Forget(key)
	// Report to an external entity that, even after several retries, we could not successfully process this key
	runtime.HandleError(err)
	glog.Infof("Dropping Object %q out of the queue: %v", key, err)
}

func (c *Controller) Run(threadiness int, stopCh chan struct{}) {
	defer runtime.HandleCrash()

	// Let the workers stop when we are done
	defer c.queue.ShutDown()
	glog.Info("Starting Kubernetes controller")

	go c.informer.Run(stopCh)

	// Wait for all involved caches to be synced, before processing items from the queue is started
	if !cache.WaitForCacheSync(stopCh, c.informer.HasSynced) {
		runtime.HandleError(fmt.Errorf("Timed out waiting for caches to sync"))
		return
	}

	for i := 0; i < threadiness; i++ {
		go wait.Until(c.runWorker, time.Second, stopCh)
	}

	<-stopCh
	glog.Info("Stopping Kubernetes controller")
}

func (c *Controller) runWorker() {
	for c.processNextItem() {
	}
}

func main() {
	flag.BoolVar(&dryRun, "dry-run", false, "Set flag to only print the operations instead of actually doing them")
	flag.Parse()

	// Convinces goflags that we have called Parse() to avoid noisy logs.
	// See https://github.com/kubernetes/kubernetes/issues/17162
	goflag.Set("logtostderr", "true")
	goflag.CommandLine.Parse([]string{})

	// creates the connection
	config, err := rest.InClusterConfig()
	if err != nil {
		glog.Fatal(err)
	}

	// creates the clientset
	clientset, err := kubernetes.NewForConfig(config)
	if err != nil {
		glog.Fatal(err)
	}

	// create the Namespace watcher
	namespaceListWatcher := cache.NewListWatchFromClient(clientset.CoreV1().RESTClient(), "namespaces", "", fields.Everything())
	fmt.Println("Watching Namespaces")

	if dryRun {
		fmt.Println("Running in dry-run mode, operations will not change data.")
	}

	// create the workqueue
	queue := workqueue.NewRateLimitingQueue(workqueue.DefaultControllerRateLimiter())

	// Bind the workqueue to a cache with the help of an informer. This way we make sure that
	// whenever the cache is updated, the Object key is added to the workqueue.
	// Note that when we finally process the item from the workqueue, we might see a newer version
	// of the Object than the version which was responsible for triggering the update.
	indexer, informer := cache.NewIndexerInformer(namespaceListWatcher, &v1.Namespace{}, 0, cache.ResourceEventHandlerFuncs{
		AddFunc: func(obj interface{}) {
			key, err := cache.MetaNamespaceKeyFunc(obj)
			if err == nil {
				queue.Add(key)
			}
		},
		UpdateFunc: func(old interface{}, new interface{}) {
			key, err := cache.MetaNamespaceKeyFunc(new)
			if err == nil {
				queue.Add(key)
			}
		},
		DeleteFunc: func(obj interface{}) {
			// IndexerInformer uses a delta queue, therefore for deletes we have to use this
			// key function.
			key, err := cache.DeletionHandlingMetaNamespaceKeyFunc(obj)
			if err == nil {
				queue.Add(key)
			}
		},
	}, cache.Indexers{})

	controller := NewController(queue, indexer, informer, clientset)

	// Now let's start the controller
	stop := make(chan struct{})
	defer close(stop)
	go controller.Run(1, stop)

	// Wait forever
	select {}
}
