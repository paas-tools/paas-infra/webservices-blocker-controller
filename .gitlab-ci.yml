variables:
  RESOURCE: ${CI_PROJECT_NAME}

stages:
  - build_go_binary
  - build_docker_image
  - test
  - deploy

# Environment definitions
.dev_environment: &dev_environment
    name: dev
    url: ${SERVER_DEV}/console/project/${NAMESPACE}

.prod_enviroment: &prod_environment
    name: prod
    url: ${SERVER_PROD}/console/project/${NAMESPACE}

### Build

# Statically compile the go binary in order to natively run in
# an empty image
build_go_binary:
  stage: build_go_binary
  image: golang:1.9
  before_script:
    - go get -u github.com/Masterminds/glide
    # In order for the vendor folder to be taken into account, the project needs to inside the GOPATH
    - mkdir -p ${GO_SRC_WORKSPACE} && cp -r . ${GO_SRC_WORKSPACE}
  script:
    - echo 'Pulling all dependencies ...'
    - cd ${GO_SRC_WORKSPACE}
    - glide update --strip-vendor
    - echo 'Building static go binary ...'
    - CGO_ENABLED=0 GOOS=linux go build -a -installsuffix cgo -o ${CI_PROJECT_DIR}/main .
  artifacts:
    paths:
      - main
  variables:
    GO_SRC_WORKSPACE: /go/src/gitlab.cern.ch/${CI_PROJECT_NAMESPACE}/${CI_PROJECT_NAME}

# Build Docker image from compiled binary in previous stage
# This image can be used to run tests
build_image_from_topic_branch:
  stage: build_docker_image
  except:
    - master
  tags:
    - docker-image-build
  script: "echo 'Building image ...'"
  variables:
    TO: ${CI_REGISTRY_IMAGE}:${CI_COMMIT_REF_NAME}

build_image_from_master:
  stage: build_docker_image
  only:
    - master
  tags:
    - docker-image-build
  script: "echo 'Building image ...'"

### Test

test:
  stage: test
  script:
    - 'echo "Not implemented!"'

.base_deploy: &base_deploy
  stage: deploy
  image: gitlab-registry.cern.ch/paas-tools/openshift-client
  script:
  - oc replace -f deploy/ --token=${TOKEN} --server=${SERVER} -n ${NAMESPACE}
  - oc tag --reference-policy=local --source=docker  ${CI_REGISTRY_IMAGE}:${oo_tag} ${RESOURCE}:deployed-from-gitlab-ci --token=${TOKEN} --server=${SERVER} -n ${NAMESPACE}

# Deploy manifests and run the new image in OpenShift
## Manual trigger to run in openshift-dev the code in the custom branch
manual_deploy_dev:
  <<: *base_deploy
  environment:
    <<: *dev_environment
  when: manual
  except:
  - master
  variables:
    SERVER: ${SERVER_DEV}
    TOKEN: ${PAAS_INFRA_SYNC_TOKEN_DEV}
    oo_tag: ${CI_COMMIT_REF_NAME}

## When merged to master, deploy directly to dev and prod
deploy_dev:
  <<: *base_deploy
  environment:
    <<: *dev_environment
  only:
  - master
  variables:
    SERVER: ${SERVER_DEV}
    TOKEN: ${PAAS_INFRA_SYNC_TOKEN_DEV}
    oo_tag: latest

deploy_prod:
  <<: *base_deploy
  environment:
    <<: *prod_environment
  only:
  - master
  variables:
    SERVER: ${SERVER_PROD}
    TOKEN: ${PAAS_INFRA_SYNC_TOKEN_PROD}
    oo_tag: latest
